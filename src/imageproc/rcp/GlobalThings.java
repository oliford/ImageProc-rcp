package imageproc.rcp;

/** the one and only 
 * For tracking all the bits, because we don't really need or want all this /plugin/ nonsense
 * We're only here for the GUI
 */
public class GlobalThings {
	private static GlobalThings instance = new GlobalThings(); 
	
	public static GlobalThings instance() { return instance; }
	
	private GlobalThings() { }
	
	public View view1;
	public View2 view2;
	public View3 view3;
	
}
