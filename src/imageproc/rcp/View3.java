package imageproc.rcp;

import java.util.*;

import javax.inject.Inject;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.contexts.IWorkbenchContextSupport;
import org.eclipse.ui.part.ViewPart;


public class View3 extends ViewPart {
	public static final String ID = "ImageProc-rcp.view3";

	@Inject IWorkbench workbench;
	
	//private ImagePanel imagePanel;
	

	public View3() {
		super();
		GlobalThings.instance().view3 = this;
	}
	
	@Override
	public void createPartControl(Composite parent) {
		
	}

	@Override
	public void setFocus() {
		
	}

	
}